package com.example.m100266177.midterm_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by 100266177 on 28/10/2015.
 */
public class ShowFeedActivity extends Activity implements StoryDataListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showfeedactivity);

        Intent intent = getIntent();
        String url = intent.getStringExtra("URL");

        DownloadFeedTask dTask = new DownloadFeedTask();
        dTask.execute(url);
    }

    @Override
    public void showStories(ArrayList<Story> data) {
        StoryAdapter story_adapter = new StoryAdapter(this, data);
        ListView listView = (ListView)findViewById(R.id.showFeedListView);
        listView.setAdapter(story_adapter);
    }
}
