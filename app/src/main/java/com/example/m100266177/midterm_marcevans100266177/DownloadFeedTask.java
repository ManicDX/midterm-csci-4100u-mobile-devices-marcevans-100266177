package com.example.m100266177.midterm_marcevans100266177;

import android.os.AsyncTask;
import android.util.Log;

import org.xml.sax.helpers.XMLReaderAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by 100266177 on 28/10/2015.
 */
public class DownloadFeedTask extends AsyncTask<String, Void, String>{
    public String data = "";
    private ArrayList<Story> storyList;

    @Override
    protected String doInBackground(String... params) {
        try
        {
            URL url = new URL(params[0]);
            HttpURLConnection conn = null;
            conn = (HttpURLConnection) url.openConnection();

            //conn.getContent();
            //Get XML stuff
            //conn.getContent();?
            //storyList.addAll();

            //storyList.addAll(conn.getContent());


            InputStream in = conn.getInputStream();
            data = getStringFromInputStream(in);
        }
        catch(IOException e)
        {
            e.printStackTrace();
            Log.e("Error", "No Connection");
        }
        return data;

    }

    //protected void onPostExecute(Result result){
    //    StoryDataListener.showStories(storyList);
    //}


    //String function thing, just in case
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
