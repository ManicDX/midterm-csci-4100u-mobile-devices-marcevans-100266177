package com.example.m100266177.midterm_marcevans100266177;

import java.util.ArrayList;

public interface StoryDataListener {
    public void showStories(ArrayList<Story> data);
}
